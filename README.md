# Open Privacy Docker Registry

We maintain a small docker registry of build images we use that can't be pulled or can't be relied upon to be pulled in the future from docker. In practice this means versioning specific docker images that aren't tagged with versions so become unabailable with updates, and where we have standardized on a specific version.

The registry is [registry.openprivacy.ca:5000](registry.openprivacy.ca:5000)

## Usage Example

```
docker pull registry.openprivacy.ca:5000/therecipe/qt:linux_static-2020.09
docker tag a00933a67efc therecipe/qt:linux_static
docker rmi registry.openprivacy.ca:5000/therecipe/qt:linux_static-2020.09
```

# Listing

- therecipe/qt:linux_static-2019.11
- therecipe/qt:linux_static-2020.01
- therecipe/qt:linux_static-2020.09 (a00933a67efc)
- therecipe/qt:windows_64_static-2019.11
- therecipe/qt:windows_64_static-2020.01
- therecipe/qt:windows_64_static-2020.09 (c86fbe0b1983)
- therecipe/qt:android-2019.11
- therecipe/qt:android-2020.01
- therecipe/qt:android-2020.09 (24c55cba591c)
